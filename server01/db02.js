// 1. 引入模块
const mongoose = require('mongoose');

// 2. 连接数据库
/*
    协议： mongodb://
    端口： 27017
    数据库的名称: 如果数据库没有，会自动创建

    { useNewUrlParser: true }  解析连接的规则
*/
const protol = 'mongodb://'
const host = '127.0.0.1'
const port = '27017'
const database = 'chenwei'

// 4. 导出一个方法
module.exports = function(success, error) {
    // 每调用一次这个函数，就会执行一次连接
    mongoose.connect(`${protol}${host}:${port}/${database}`, { useNewUrlParser: true })

    // 3. 监听成功和失败的事件处理
    mongoose.connection.on('open', () => {
        // if(success) {
        //     success()
        // }
        // if(success) success();
        // success && success();
        success?.();
    })

    mongoose.connection.on('error', () => {
       if(error) {
            error()
       }
    })
}