// 1. 引入第三方模块
const mongoose = require("mongoose");

// 2. 创建数据库的连接
mongoose.connect('mongodb://127.0.0.1:27017/guigu')

// 3. 设置成功或者失败的监听
mongoose.connection.on('open', () => {
    console.log('连接成功');

    // 3.1 创建集合的规范
    //  Schema  [ˈskimə]  关于一个集合的字段描述
    const userSchema = mongoose.Schema({
        // 属性: 数据类型（构造函数名称）
        name: String,
        age: Number,
        hobby: Array,
        addr: String
    })

    // 3.2 创建模型的对象
    const dbUser = mongoose.model('user', userSchema)

    // 3.3 插入数据
    // 参数1: 添加的数组    参数2: 回调函数
    // dbUser.insertMany([
    //     { name: '迪丽热巴', age: 20, hobby: [], addr: '新疆' },
    //     { name: '古力娜扎', age: 22, hobby: [], addr: '新疆' },
    //     { name: '马尔扎哈', age: 40, hobby: [], addr: '峡谷' }
    // ], (err, data) => {
    //     if(err) throw err

    //     console.log(data)
    // })

    // 3.4 删除数据
    // dbUser.deleteOne({ age: 20 }, err => {
    //     // throw 在捕获到错误后会阻塞后续的执行
    //     if(err) throw err
    //     console.log('删除成功');
    // })
    // 当条件相同时，会删除多个
    // dbUser.deleteMany({ age: 22 }, err => {
    //     // throw 在捕获到错误后会阻塞后续的执行
    //     if(err) throw err
    //     console.log('删除成功');
    // })
    // 当出现大于小于的时候，满足条件的都删掉
    // dbUser.deleteMany({ age: { $lt: 18 } }, err => {
    // dbUser.deleteMany({ age: { $lte: 18 } }, err => {
    //     if(err) throw err
    //     console.log('删除多项成功');
    // })

    // 3.5 更新数据
    // dbUser.updateOne({ age: 22 }, { addr: '北京' }, err => {
    //     if(err) throw err;
    //     console.log('更新成功!');
    // })
    // dbUser.updateMany({ age: { $gte: 30 } }, { addr: '臭水沟' }, err => {
    //     if(err) throw err;

    //     console.log('批量修改成功!');
    //     mongoose.connection.close();
    // })

    // 3.6 查询数据 - 基本查询
    // dbUser.find({}, (err, data) => {
    //     if(err) throw err

    //     console.log(data);
    //     mongoose.connection.close();
    // })
    // 带一些基本条件
    // dbUser.find({ age: { $gte: 30 } }, (err, data) => {
    //     if(err) throw err
    //     console.log(data);
    //     mongoose.connection.close();
    // })
    // dbUser.findOne({ addr: '臭水沟' }, (err, data) => {
    //     if(err) throw err
    //     console.log(data);
    //     mongoose.connection.close();
    // })
    // dbUser.findById('6368aae48ac76722fc860bc6', (err, data) => {
    //     if(err) throw err;
    //     console.log(data);
    //     mongoose.connection.close();
    // })
    // 找到这个id对应的数据并且修改
    // const id = '6368aae48ac76722fc860bc6'
    // dbUser.findByIdAndUpdate(id, {addr: '峡谷'}, err => {
    //     if(err) throw err;

    //     console.log('修改成功！');
    //     mongoose.connection.close();
    // })

    // 高级查询  1 升序  -1  降序  exec  提取数据
    // dbUser.find().sort({ age: 1 }).exec((err, data) => {
    //     if(err) throw err;
    //     console.log(data);
    //     mongoose.connection.close();
    // })
    // 筛选 - 使数据简化 - id需要单独设置  1 保留  0 不保留
    // const sel = { name: 1, age: 1, addr: 1, _id: 0 }
    // dbUser.find().select(sel).sort({ age: 1 }).exec((err, data) => {
    //     if(err) throw err;
    //     console.log(data);
    //     mongoose.connection.close();
    // })
    // 跳过
    // const sel = { name: 1, age: 1, addr: 1, _id: 0 }
    // dbUser.find().select(sel).sort({ age: 1 }).skip(2).exec((err, data) => {
    //     if(err) throw err;
    //     console.log(data);
    //     mongoose.connection.close();
    // })
    // 截取
    const sel = { name: 1, age: 1, addr: 1, _id: 0 }
    // dbUser.find().select(sel).sort({ age: 1 }).skip(4).limit(2).exec((err, data) => {
    //     if(err) throw err;
    //     console.log(data);
    //     mongoose.connection.close();
    // })

    // 分页的逻辑
    /*
        客户端会传字段，currentPage 当前页， pageSize 一页展示多少数据
        currentPage = 3   pageSize = 8
        skip((currentPage - 1) * pageSize).limt(pageSize)
    */

    // 获取模糊匹配的字段
    const str = '扎'
    const reg = new RegExp(str, 'ig');  //  /扎/ig  i 不区分大小写  g  全局匹配
    dbUser.find({ name: reg }).select(sel).exec((err, data) => {
        if(err) throw err;

        console.log(data);
        mongoose.connection.close()
    })
})

mongoose.connection.on('error', () => {
    console.log('连接失败');
})