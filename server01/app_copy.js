/*
    说明：
        这是项目的启动文件
            在这个文件中，创建调用数据库的连接和集合声明
            获取模型对象，在请求之后，操作数据库
*/

// 1. 引入模块
const heros = require('./model.js')
// 引入的这个对象，只能在连接数据库成功之后才能操作
const db = require('./db02.js')
// 断开数据库的连接必须要mongoose
const mongoose = require('mongoose')


// 传入实参 - 回调函数
db(
    // 专门用于处理成功的回调函数
    () => {
        console.log('启用了，数据库连接成功了');

        // 成功处理之后的回调 - 操作数据库  CRUD
        heros.insertMany(
            [
                { name: '鲁班七号', age: 20, skill: ['大炮', '手枪'] },
                { name: '李白', age: 22, skill: ['到处跑', '画圈'] }
            ],
            err => {
                if(err) throw err;

                console.log('数据添加成功~');
                mongoose.connection.close();
            } 
        )
    }, 
    // 失败的处理
    () => {
        console.log('error, connect failed..');
    }
);